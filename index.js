var app = require('app');
var BrowserWindow = require('browser-window');

var mainWindow = null;

app.on('window-all-closed', function() {
  if (process.platform != 'darwin')
    app.quit();
});

app.on('ready', function() {
  var config = {
    show: true,
    resizable: false,
    frame: false,
    'enable-larger-than-screen': true,
    'web-preferences': {
      'web-security': false,
      'plugins': true
    },
    'always-on-top': true,
    width: 360,
    height: 460
  };

  mainWindow = new BrowserWindow(config);
  mainWindow.loadUrl('file://' + __dirname + '/app/index.html');
  mainWindow.on('closed', function() {
    mainWindow = null;
  });
});
